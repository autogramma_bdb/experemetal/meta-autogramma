# Remove unneeded files
SRC_URI_remove = "file://fw_env.config"

# Change source code URL
UBOOT_SRC = "git://gitlab.com/Autogramma_BDB/uboot-imx.git;protocol=http"
SRCBRANCH_ag-bdb-mx6 = "imx_v2018.03_4.14.78_1.0.0_ga_var02"
SRCREV_ag-bdb-mx6 = "${AUTOREV}"

# etc
UBOOT_INITIAL_ENV_DEVICE_ag-bdb-mx6 = "nand"

