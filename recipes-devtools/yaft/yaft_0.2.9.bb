SUMMARY = "Yet Another Framebuffer Terminal"
AUTHOR = "uobikiemukot"
DESCRIPTION = "Yet Another Framebuffer Terminal"
HOMEPAGE = "https://github.com/uobikiemukot/yaft"

LICENSE = "MIT"
LIC_FILES_CHKSUM="file://LICENSE;md5=0a1f0030369f9d1f26870e0bfc24c47b"
RDEPENDS_${PN} = ""
DEPENDS += "${RDEPENDS_${PN}} ncurses-native "
BRANCH = "master"
SRCREV = "59ef091187736200e07ee1d67d6249ad4c691542"
SRC_URI = "git://github.com/uobikiemukot/yaft.git;protocol=git;branch=${BRANCH} \
	file://glyph.patch file://glyph.h"
FILESEXTRAPATHS_prepend = "${THISDIR}/${PN}:"
S = "${WORKDIR}/git"

do_compile() {
	cp ../glyph.h .
	make
}

do_install() {
	make install DESTDIR=${D}
}

FILES_${PN} = " /usr/share/terminfo/y /usr/bin/yaft /usr/bin/yaft_wall "
