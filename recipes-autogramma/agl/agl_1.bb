SUMMARY = "BDB Dashboard"
AUTHOR = "Autogramma"
DESCRIPTION = "BDB Dashboard application in Qt Quick"
HOMEPAGE = "https://www.autogramma.ru"
LICENSE = "CLOSED"
DEPENDS = "libsdl2 libpng"
RDEPENDS_${PN} = "libsdl2 libpng"
SRC_URI = "file://./"
S = "${WORKDIR}"

inherit cmake systemd pkgconfig

EXTRA_OECMAKE = "\
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DCMAKE_INSTALL_PREFIX:PATH=/opt/app/current/ \
  -DUSE_SDL=ON \
  -DOUTPUT_MAP=ON \
  -DAPP_VARIANT=ForeignElectrobus \
"

do_install_append() {
  install -m 0755 ${S}/setup.sh ${D}/opt/app/current/setup.sh

  install -d ${D}${systemd_unitdir}/system
  install -m 0644 ${S}/app.service        ${D}${systemd_unitdir}/system/

  mkdir -p ${D}${sysconfdir}/systemd/system/local-fs.target.wants/

  ln -sf ${systemd_unitdir}/system/app.service \
     ${D}${sysconfdir}/systemd/system/local-fs.target.wants/app.service
}

FILES_${PN} += "/opt/app/current ${systemd_unitdir}/system/ ${sysconfdir}/systemd/system/local-fs.target.wants"

# временный хак, чтобы исправить ошибку
# "File /opt/dashboard/bin/dashboard from dashboard was already stripped, this will prevent future debugging! [already-stripped]"
# INSANE_SKIP_${PN}_append = "already-stripped"
