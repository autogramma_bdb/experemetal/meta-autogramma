#!/bin/bash

#################################################################
#                                                               #
# Скрипт позволяет автоматизировать часто повторяющиеся задачи. #
# Все сценарии в данном файле рассчитаны на сборку Android для  #
# устройства МК монитора.                                       #
#                                                               #
#################################################################

_IMAGE=ag-image-gui
_MACHINE=ag-bdb-mx6
_MACHINE_=ag_bdb_mx6
_DISTRO=fslc-framebuffer-ag
_BUILD_DIR=build_fb

items=() # Массив ключей и текстов для пунктов меню.
nodes=() # Массив блочных устройств и их размеров.

# Обновляет массив nodes.
nodes_array_update() {
	nodes=( $(lsblk | grep -E 'sd[a-z] ' | awk '{ print $1, "[" $4 "]" }') )
}

# Возвращает последнее блочное устройство, доступное в системе.
get_last_node() {
	nodes_array_update
	echo ${nodes[${#nodes[@]}-2]}
}

# item_add <тег> <текст_пункта_меню>
# Добавляет новый элемент в в меню.
item_add() {
	local menu_index=$1
	local title=$2

	local index=$(( $menu_index * 2 ))
	items[$index]=$1
	items[$index+1]="$title"
}

# item_title_by_index <тег_пункта_меню>
# Возвращает текст пункта меню по его тегу.
item_title_by_index() {
	echo ${items[$1*2+1]}
}

# Отображает меню доступных сценариев.
# Возвращает тег выбранного пользователем  пункта меню.
show_menu() {
	# Количество пунктов меню в массиве.
	# В массиве записаны последовательно индекс элемент - текст - индекс...,
	#	поэтому размер массива делим пополам.
	local count=$(( ${#items[@]} / 2 ))

	# Список элементов массива.
	local items_list="${items[@]}"

	local cols=79
	local rows=$(( $count + 7 ))
	
	dialog --title "$title" --default-item $LAST_ITEM --menu "Выберите один из пунктов:" $rows $cols $count "${items[@]}" 2>&1 >/dev/tty
}

# Отображает меню выбора блочных устройств.
# Возвращает выбранное пользователем блочное устройство.
select_block_device() {
	local count=$(( ${#nodes[@]} / 2 ))
	local items_list="${nodes[@]}"
	local title="Выберите блочное устройство для записи на SD-карту"
	local cols=79
	local rows=$(( $count + 7 ))

	nodes_array_update
	dialog --title "$title" --default-item $LAST_NODE --menu "Выберите один из пунктов:" $rows $cols $count "${nodes[@]}" 2>&1 >/dev/tty
}

# Инициализирует переменные для сборки
script_setup_build_env() {
	local RETURNDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

	export MACHINE=$_MACHINE
	export DISTRO=$_DISTRO

	source ./setup-environment $_BUILD_DIR
}

# По идее пишит спикером, но на деле выводит уведомление от терминала
beep() {
	echo -e '\a'
}

script_build() {
	#bitbake -c cleanall core-image-minimal не уверен что работет как надо
	#bitbake -c cleansstate virtual/bootloader virtual/kernel эта штука работат
	#bitbake core-image-minimal
	#bitbake core-image-minimal -c deploy тестить надо
	#bitbake core-image-minimal -c compile -f тестить надо
	#bitbake core-image-minimal -c imx_v7_var_defconfig -f Собрать конкретный конфиг ядра
	
	#Без этого образы могуть собиратся надекватно.
	rm -rdf ${PWD}/out_iso 
	bitbake -c cleanall agl
	bitbake -c cleanall "$_IMAGE"
	time bitbake "$_IMAGE"
	cd ../
}

script_out_extrainfo() {
	local DEST_INFO=$1
	rm -rf $DEST_INFO
	mkdir -p $DEST_INFO
	touch $DEST_INFO/git_hash.sh
	touch $DEST_INFO/list_files.txt
	touch $DEST_INFO/list_packages.txt
	echo '#!/bin/bash' > $DEST_INFO/git_hash.sh
	sed -e '1d; s/SRCREV = /export rev_kernel=/' ./$_BUILD_DIR/buildhistory/packages/$_MACHINE_-fslc-linux-gnueabi/linux-variscite/latest_srcrev >> $DEST_INFO/git_hash.sh
	sed -e '1d; s/SRCREV = /export rev_uboot=/' ./$_BUILD_DIR/buildhistory/packages/$_MACHINE_-fslc-linux-gnueabi/u-boot-variscite/latest_srcrev >> $DEST_INFO/git_hash.sh
	sed -e '1,13d;15d;21d;22d;27,28d;s/ \{1,\}//;s/ //;s/HEAD://;s/thud://' ./$_BUILD_DIR/buildhistory/images/$_MACHINE_/glibc/$_IMAGE/build-id.txt | sed -e 's/^/export /;s/-/_/;s/-/_/' >> $DEST_INFO/git_hash.sh
	agl_git=`git --git-dir ./sources/meta-autogramma/recipes-autogramma/agl/files/.git rev-parse HEAD`
	echo "export agl=\"$agl_git\"" >> $DEST_INFO/git_hash.sh
	chmod a+x $DEST_INFO/git_hash.sh

	cp ./$_BUILD_DIR/buildhistory/images/$_MACHINE_/glibc/$_IMAGE/files-in-image.txt $DEST_INFO/list_files.txt
	cp ./$_BUILD_DIR/buildhistory/images/$_MACHINE_/glibc/$_IMAGE/installed-packages.txt $DEST_INFO/list_packages.txt
}

script_out_additions() {
	local DEST=$1
	script_out_extrainfo $DEST/info
	cp ./scripts/other_tools/out_additions/* $DEST
}

menu_item_1_selected() {
	script_build
}

menu_item_2_selected() {
	bitbake -c cleanall u-boot-variscite linux-variscite
	#костыля для косяка сборки извлеченного devtool-ом ядра.
	#sync
	#rm -rf ${PWD}/tmp/work-shared/$_MACHINE/kernel-source/
	#ln -s ${PWD}/workspace/sources/linux-variscite ${PWD}/tmp/work-shared/$_MACHINE/kernel-source 
	#cd ../
}

menu_item_3_selected() {
	cd ../
	mkdir -pv ${PWD}/out_iso
	mkdir -pv ${PWD}/extra_info
	script_out_extrainfo ${PWD}/extra_info
	${PWD}/sources/meta-autogramma/yocto_iso_creator.sh ${PWD}/$_BUILD_DIR/tmp/deploy/images/$_MACHINE/ ${PWD}/out_iso ${PWD}/extra_info
	rm -rf ${PWD}/extra_info
	script_out_additions ${PWD}/out_iso
}

menu_item_4_selected() {
	repo sync -j1 -v
}

menu_item_5_selected() {

	if [[ $UID != "0" ]]; then
	    echo " "
	    echo "*******************************************************************************"
	    echo "Необходимы привелигии администратора для работы скрипта"
	    echo "*******************************************************************************"
	    echo " "
	fi
	
	sudo rm -f /etc/xinetd.d/tftp
	sudo touch /etc/xinetd.d/tftp
	sudo chmod -R 777 /etc/xinetd.d/tftp
	
	#это временное решение надо править стоковые env u-boot
	pushd ${PWD}/tmp/deploy/images/$_MACHINE/
	ln -s -f imx6q-ag-bdb-$_MACHINE.dtb  imx6q-var-som-res.dtb
	ln -s -f imx6dl-ag-bdb-$_MACHINE.dtb imx6dl-var-som-res.dtb
	popd
	
	echo "service tftp" >> /etc/xinetd.d/tftp
	echo "{" >> /etc/xinetd.d/tftp
	echo "     protocol = udp" >> /etc/xinetd.d/tftp
	echo "     port = 69" >> /etc/xinetd.d/tftp
	echo "     socket_type = dgram" >> /etc/xinetd.d/tftp
	echo "     wait = yes" >> /etc/xinetd.d/tftp
	echo "     user = nobody" >> /etc/xinetd.d/tftp
	echo "     server = /usr/sbin/in.tftpd" >> /etc/xinetd.d/tftp
	echo "     server_args = ${PWD}/tmp/deploy/images/$_MACHINE" >> /etc/xinetd.d/tftp
	echo "     disable = no" >> /etc/xinetd.d/tftp
	echo "}" >> /etc/xinetd.d/tftp
	
	sudo service xinetd restart

	HOST_IP=`ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`
	HOST_NET=`echo $HOST_IP | rev | cut -d. -f2- | rev`
	
	echo "*******************************************************************************"
	echo "Пакеты необходимы для работы сервера:"
	echo " sudo apt-get install xinetd tftpd tftp"
	echo " sudo service xinetd restart"
	echo " sudo apt-get install nfs-kernel-server"
	echo " "
	echo " Руками добаляем в файл /etc/exports:"
	echo " $PWD/tmp/deploy/images/nfs_root_mnt ${HOST_IP}0/24(fsid=0,no_subtree_check)"
	echo " $PWD/tmp/deploy/images/nfs_root_mnt/root_fs ${HOST_IP}0/24(rw,sync,no_subtree_check,crossmnt,no_root_squash)"
	echo " обновляем список экспортов: sudo exportfs -a"
	echo " "
	echo "Как грузится:"
	echo " 1) ловим загрузчик"
	echo " 2) прописываем IP хост машины в аргументы u-boot и СТАРТУЕМ!!! :"
	echo "  setenv serverip $HOST_IP"
	echo "  run netboot"
	echo " "
	echo "*******************************************************************************"
	
	mkdir -p ${PWD}/tmp/deploy/images/nfs_root_mnt
	mkdir -p ${PWD}/tmp/deploy/images/nfs_root_mnt/root_fs
	sudo chown nobody:nogroup ${PWD}/tmp/deploy/images/nfs_root_mnt
	sudo chown nobody:nogroup ${PWD}/tmp/deploy/images/nfs_root_mnt/root_fs
	
	sleep 1
	sync
	#хз почему но на 3-4 раз перестёт грузится с сервера это вроде помогает
	sudo service nfs-kernel-server restart
	sleep 1
    
	echo " "
	echo " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! "
	echo " "
	echo " Чтобы избежать колапсов фаловых систем скрит блокирует процесс"
	echo " Сейчас root_fs примаунченный через NFS будет доступен в локальной подсети"
	echo " "
	echo "*******************************************************************************"
	echo " "
	sync
	sleep 3
	
	sudo mount -o loop ${PWD}/tmp/deploy/images/$_MACHINE/$_IMAGE-$_MACHINE.ext4 ${PWD}/tmp/deploy/images/nfs_root_mnt/root_fs
	
	sync
	echo " Нажмите ENTER чтобы прекратить блокировку и отмаунтить root_fs "
	echo "              для ручного отмаунчивания юзай: "
	read -p " sudo umount -dl ${PWD}/tmp/deploy/images/nfs_root_mnt/root_fs "
	sleep 2
	sync
	sudo umount -dl ${PWD}/tmp/deploy/images/nfs_root_mnt/root_fs
	sync
	cd ../
}

menu_item_q_selected() {
	cd ../
}

# Точка входа в сценарий.
main() {
	# Если переменные окружения не были настроены, то настраиваем.
	if [ -z "${DISTRO}" ]; then script_setup_build_env; fi

	if [ -z "${LAST_ITEM}" ]; then LAST_ITEM="0"; fi
	if [ -z "${LAST_NODE}" ]; then LAST_NODE="$(get_last_node)"; fi

	item_add 1 "Сборка дистрибутива Yocto"
	item_add 2 "Очистка сборки Yocto(нужно при изменениях ядра/загрузчика)"
	item_add 3 "Сборка образа для запуска образа с SD"
	item_add 4 "Синхронизация дерева исходного кода(repo)"
	item_add 5 "Service: Инициализировать TFTP для сетевой загрузки ядра"
	item_add q "Выйти"
	LAST_ITEM=$(show_menu)

	if [ -z "${LAST_ITEM}" ]; then LAST_ITEM='q'; fi
	clear
	menu_item_${LAST_ITEM}_selected
	beep # Показать уведомление, что мы завершили работу
}

main
