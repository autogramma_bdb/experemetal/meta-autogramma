# Copyright (C) 2015 Freescale Semiconductor
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Autogramma image with GUI, based on fsl-image-gui"
LICENSE = "MIT"

inherit core-image features_check

IMAGE_FEATURES += " \
    ssh-server-openssh \
    hwcodecs \
    debug-tweaks \
    nfs-server \
    tools-debug \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', '', \
       bb.utils.contains('DISTRO_FEATURES',     'x11', 'x11-base x11-sato', \
                                                       '', d), d)} \
"

CORE_IMAGE_EXTRA_INSTALL += " \
	packagegroup-imx-tools-audio \
	packagegroup-fsl-gstreamer1.0 \
	packagegroup-fsl-gstreamer1.0-full \
	${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'xterm', '', d)} \
	${@bb.utils.contains('DISTRO_FEATURES', 'x11 wayland', 'weston-xwayland', '', d)} \
	${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'weston-init', \
	   bb.utils.contains('DISTRO_FEATURES',     'x11', 'packagegroup-core-x11-sato-games', \
							 '', d), d)} \
	screen \
	tcf-agent \
	gcc-sanitizers \
	libsdl2 \
	util-linux-partx \
	openssh openssh-sftp-server openssh-sftp openssh-misc yaft \
	openssl \
	minicom \
	u-boot-variscite \
"

# only for DRM enabled machines
IMAGE_INSTALL_append_imxdrm = " \
	libdrm-tests \
"

CORE_IMAGE_EXTRA_INSTALL_append_mx8 = "\
    packagegroup-fsl-tools-gpu \
"

# Due to the Qt samples the resulting image will not fit the default NAND size.
# Removing default ubi creation for this image
IMAGE_FSTYPES_remove = "multiubi"

# Нам удобнее использовать ext4 образы, чем tar.gz
IMAGE_FSTYPES_append = " ext4 "

systemd_disable_vt () {
    rm ${IMAGE_ROOTFS}${root_prefix}${sysconfdir}/systemd/system/getty.target.wants/getty@tty*.service
}

IMAGE_PREPROCESS_COMMAND_append = " ${@ 'systemd_disable_vt;' if bb.utils.contains('DISTRO_FEATURES', 'systemd', True, False, d) and bb.utils.contains('USE_VT', '0', True, False, d) else ''} "
