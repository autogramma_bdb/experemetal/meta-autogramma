# Copyright (C) 2013-2016 Freescale Semiconductor
# Copyright 2017 NXP
# Copyright 2018-2019 Variscite Ltd.
# Released under the MIT license (see COPYING.MIT for the terms)

DEPENDS_append = " lz4-native "

LOCALVERSION_ag-bdb-mx6 = "-imx6ul"
LINUX_VERSION = "5.4.85"
SRCBRANCH = "5.4-2.1.x-imx_var01"
SRCREV = "${AUTOREV}"
KERNEL_SRC = "git://gitlab.com/Autogramma_BDB/linux_kernel.git;protocol=http"
KBUILD_DEFCONFIG_ag-bdb-mx6 = "imx_v7_var_defconfig"
